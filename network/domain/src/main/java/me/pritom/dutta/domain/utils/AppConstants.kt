package me.pritom.dutta.domain.utils

/**
 * Created by Pritom Dutta on 22/3/24.
 */
object AppConstants {
    const val Q = "android"
    const val SORT = "stars"

    //Date Format
    const val yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss"
    const val MM_dd_yyyy_HH_ss = "MM-dd-yyyy HH:ss"
}