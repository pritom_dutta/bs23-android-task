package me.pritom.dutta.domain.repository

import kotlinx.coroutines.flow.Flow
import me.pritom.dutta.domain.model.RepositoryResponse
import me.pritom.dutta.domain.utils.NetworkResult

/**
 * Created by Pritom Dutta on 22/3/24.
 */
interface RepoListRepository {
    suspend fun fetchListRepository(packageId: String): Flow<NetworkResult<RepositoryResponse>>
}