package me.pritom.dutta.domain.model

data class RepositoryResponse(
    val incomplete_results: Boolean?,
    val items: List<RepoItem>?,
    val total_count: Int?
)