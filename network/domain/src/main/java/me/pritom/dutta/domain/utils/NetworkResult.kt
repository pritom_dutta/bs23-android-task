package me.pritom.dutta.domain.utils

/**
 * Created by Pritom Dutta on 22/3/24.
 */
sealed class NetworkResult<out T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : NetworkResult<T>(data)
    class Error<T>(message: String?, data: T? = null) : NetworkResult<T>(data, message)
    class Loading<T> : NetworkResult<T>()
}
