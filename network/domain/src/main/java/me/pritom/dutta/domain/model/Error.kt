package me.pritom.dutta.domain.model


/**
 * Created by Pritom Dutta on 23/3/24.
 */

data class Error(
    val message: String? = null,
)