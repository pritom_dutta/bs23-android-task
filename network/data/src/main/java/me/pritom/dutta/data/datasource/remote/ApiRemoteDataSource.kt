package me.pritom.dutta.data.datasource.remote

import pritom.dutta.data.test.OpenForTesting
import me.pritom.dutta.data.utils.onResponse
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 22/3/24.
 */
@OpenForTesting
class ApiRemoteDataSource @Inject constructor(private val api: ApiService) {

    suspend operator fun invoke(
        headerPackage: String,
        q: String,
        sort: String,
    ) = api.fetchRepositoryList(headerPackage, q, sort)
        .onResponse()
}