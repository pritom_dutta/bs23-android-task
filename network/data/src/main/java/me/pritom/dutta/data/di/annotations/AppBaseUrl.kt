package me.pritom.dutta.data.di.annotations

import javax.inject.Qualifier

/**
 * Created by Pritom Dutta on 22/3/24.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class AppBaseUrl