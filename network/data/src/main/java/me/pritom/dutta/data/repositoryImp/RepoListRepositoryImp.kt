package me.pritom.dutta.data.repositoryImp

import kotlinx.coroutines.flow.Flow
import me.pritom.dutta.data.datasource.remote.ApiRemoteDataSource
import me.pritom.dutta.data.utils.onException
import me.pritom.dutta.domain.model.RepositoryResponse
import me.pritom.dutta.domain.repository.RepoListRepository
import me.pritom.dutta.domain.utils.AppConstants
import me.pritom.dutta.domain.utils.NetworkResult
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 22/3/24.
 */
class RepoListRepositoryImp @Inject constructor(
    private val remoteDataSource: ApiRemoteDataSource,
) : RepoListRepository {
    override suspend fun fetchListRepository(packageId: String): Flow<NetworkResult<RepositoryResponse>> {

        val res = remoteDataSource.invoke(
            headerPackage = packageId,
            q = AppConstants.Q,
            sort = AppConstants.SORT
        )
        return res.onException()
    }


}