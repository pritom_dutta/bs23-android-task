package me.pritom.dutta.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.pritom.dutta.data.repositoryImp.RepoListRepositoryImp
import me.pritom.dutta.domain.repository.RepoListRepository
import javax.inject.Singleton

/**
 * Created by Pritom Dutta on 22/3/24.
 */

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideRepoListRepository(api: RepoListRepositoryImp): RepoListRepository

}