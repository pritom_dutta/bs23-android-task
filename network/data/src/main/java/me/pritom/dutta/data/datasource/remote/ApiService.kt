package me.pritom.dutta.data.datasource.remote

import kotlinx.coroutines.flow.Flow
import me.pritom.dutta.domain.model.RepositoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Pritom Dutta on 22/3/24.
 */
interface ApiService {

    @GET("/search/repositories")
    suspend fun fetchRepositoryList(
        @Header("package") headerPackage: String,
        @Query("q") q: String,
        @Query("sort") sort: String,
    ): Response<RepositoryResponse>
}