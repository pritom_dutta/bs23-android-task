package me.pritom.dutta.data.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.pritom.dutta.data.di.annotations.AppBaseUrl
import javax.inject.Singleton

/**
 * Created by Pritom Dutta on 22/3/24.
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @AppBaseUrl
    fun provideBaseUrl(): String = "https://api.github.com"
}