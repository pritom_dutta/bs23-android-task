package me.pritom.dutta.data.datasource

import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import me.pritom.dutta.data.datasource.remote.ApiRemoteDataSource
import me.pritom.dutta.data.datasource.remote.ApiService
import me.pritom.dutta.domain.testutils.TestUtils
import me.pritom.dutta.domain.utils.AppConstants
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Pritom Dutta on 23/3/24.
 */
@RunWith(MockitoJUnitRunner::class)
class ApiRemoteDataSourceTest {

    @get:Rule
    val mockWebServer = MockWebServer()

    private lateinit var mockApiService: ApiService

    private lateinit var sutApiRemoteDataSource: ApiRemoteDataSource

    @Before
    fun setup() {
        val moshi = Moshi.Builder()
            .build()

        mockApiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(getOkHttpClient())
            .build()
            .create(ApiService::class.java)

        sutApiRemoteDataSource = ApiRemoteDataSource(mockApiService)
    }

    @After
    fun shutDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get repository info and correct repository list size returned`() = runBlocking {
        mockWebServer.enqueue(me.pritom.dutta.domain.testutils.TestUtils.mockResponse("api_response_data.json"))
        val data = sutApiRemoteDataSource(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).first()
        Assert.assertEquals(ApiServiceTest.LIST_SIZE, data.items?.size)
    }

    @Test
    fun `get zero repository list size returned`() = runBlocking {
        mockWebServer.enqueue(me.pritom.dutta.domain.testutils.TestUtils.mockResponse("api_response_data_0_size.json"))
        val data = sutApiRemoteDataSource(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).first()
        Assert.assertEquals(0, data.items?.size)
    }

    @Test
    fun `get repository info and correct repository list data returned`() = runBlocking {
        mockWebServer.enqueue(TestUtils.mockResponse("api_response_data.json"))
        val data = sutApiRemoteDataSource(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).first()

        Assert.assertEquals(ApiServiceTest.INDEX_0_FULL_NAME, data.items?.get(0)?.full_name ?: "")
        Assert.assertEquals(
            ApiServiceTest.INDEX_0_IMAGE,
            data.items?.get(0)?.owner?.avatar_url ?: ""
        )
        Assert.assertEquals(
            ApiServiceTest.INDEX_0_DESCRIPTION,
            data.items?.get(0)?.description ?: ""
        )

        Assert.assertEquals(ApiServiceTest.INDEX_29_FULL_NAME, data.items?.get(29)?.full_name ?: "")
        Assert.assertEquals(
            ApiServiceTest.INDEX_29_IMAGE,
            data.items?.get(29)?.owner?.avatar_url ?: ""
        )
        Assert.assertEquals(
            ApiServiceTest.INDEX_29_DESCRIPTION,
            data.items?.get(29)?.description ?: ""
        )
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .dispatcher(Dispatcher(TestUtils.immediateExecutorService()))
            .retryOnConnectionFailure(true).build()
    }
}