package me.pritom.dutta.data.repository

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import me.pritom.dutta.data.datasource.ApiServiceTest
import me.pritom.dutta.data.datasource.remote.ApiRemoteDataSource
import me.pritom.dutta.data.repositoryImp.RepoListRepositoryImp
import me.pritom.dutta.domain.testutils.TestUtils
import me.pritom.dutta.domain.utils.AppConstants
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Pritom Dutta on 23/3/24.
 */
@RunWith(MockitoJUnitRunner::class)
class RepoListRepositoryImpTest {
    @Mock
    lateinit var sutApi: ApiRemoteDataSource

    private lateinit var repoListRepositoryImp: RepoListRepositoryImp

    @Before
    fun setUp() {
        repoListRepositoryImp = RepoListRepositoryImp(sutApi)
    }

    @Test
    fun `get repository info and correct repository list size returned`() = runBlocking {
        success()

        val data = sutApi(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).first()
        Assert.assertEquals(ApiServiceTest.LIST_SIZE, data.items?.size)
    }

    @Test
    fun `get repository info and correct repository list data returned`() = runBlocking {
        success()
        val data = sutApi(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).first()

        Assert.assertEquals(ApiServiceTest.INDEX_0_FULL_NAME, data.items?.get(0)?.full_name ?: "")
        Assert.assertEquals(
            ApiServiceTest.INDEX_0_IMAGE,
            data.items?.get(0)?.owner?.avatar_url ?: ""
        )
        Assert.assertEquals(
            ApiServiceTest.INDEX_0_DESCRIPTION,
            data.items?.get(0)?.description ?: ""
        )

        Assert.assertEquals(ApiServiceTest.INDEX_29_FULL_NAME, data.items?.get(29)?.full_name ?: "")
        Assert.assertEquals(
            ApiServiceTest.INDEX_29_IMAGE,
            data.items?.get(29)?.owner?.avatar_url ?: ""
        )
        Assert.assertEquals(
            ApiServiceTest.INDEX_29_DESCRIPTION,
            data.items?.get(29)?.description ?: ""
        )
    }

    private suspend fun success() {
        val testData = TestUtils.getTestData("api_response_data.json")
        Mockito.`when`(
            sutApi.invoke(
                headerPackage = "testing.header.pass",
                q = AppConstants.Q,
                sort = AppConstants.SORT,
            )
        ).thenReturn(
            flow {
                emit(testData)
            }
        )
    }

}