package me.pritom.dutta.data.datasource

import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import me.pritom.dutta.data.datasource.remote.ApiRemoteDataSource
import me.pritom.dutta.data.datasource.remote.ApiService
import me.pritom.dutta.domain.testutils.TestUtils
import me.pritom.dutta.data.utils.onResponse
import me.pritom.dutta.domain.utils.AppConstants
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import org.junit.Assert.assertEquals

/**
 * Created by Pritom Dutta on 23/3/24.
 */
@RunWith(MockitoJUnitRunner::class)
class ApiServiceTest {

    companion object {
        const val LIST_SIZE = 30

        const val INDEX_0_FULL_NAME = "flutter/flutter"
        const val INDEX_0_IMAGE = "https://avatars.githubusercontent.com/u/14101776?v=4"
        const val INDEX_0_DESCRIPTION =
            "Flutter makes it easy and fast to build beautiful apps for mobile and beyond"

        const val INDEX_29_FULL_NAME = "2dust/v2rayNG"
        const val INDEX_29_IMAGE = "https://avatars.githubusercontent.com/u/31833384?v=4"
        const val INDEX_29_DESCRIPTION =
            "A V2Ray client for Android, support Xray core and v2fly core"
    }

    @get:Rule
    val mockWebServer = MockWebServer()

    private lateinit var sutApiService: ApiService

    @Before
    fun setup() {
        val moshi = Moshi.Builder()
            .build()

        sutApiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(getOkHttpClient())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun shutDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get repository info and correct repository list size returned`() = runBlocking {
        mockWebServer.enqueue(TestUtils.mockResponse("api_response_data.json"))
        val data = sutApiService.fetchRepositoryList(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).onResponse().first()
        assertEquals(LIST_SIZE, data.items?.size)
    }

    @Test
    fun `get zero repository list size returned`() = runBlocking {
        mockWebServer.enqueue(me.pritom.dutta.domain.testutils.TestUtils.mockResponse("api_response_data_0_size.json"))
        val data = sutApiService.fetchRepositoryList(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).onResponse().first()
        assertEquals(0, data.items?.size)
    }

    @Test
    fun `get repository info and correct repository list data returned`() = runBlocking {
        mockWebServer.enqueue(me.pritom.dutta.domain.testutils.TestUtils.mockResponse("api_response_data.json"))
        val data = sutApiService.fetchRepositoryList(
            headerPackage = "testing.header.pass",
            q = AppConstants.Q,
            sort = AppConstants.SORT,
        ).onResponse().first()

        assertEquals(INDEX_0_FULL_NAME, data.items?.get(0)?.full_name ?: "")
        assertEquals(INDEX_0_IMAGE, data.items?.get(0)?.owner?.avatar_url ?: "")
        assertEquals(INDEX_0_DESCRIPTION, data.items?.get(0)?.description ?: "")

        assertEquals(INDEX_29_FULL_NAME, data.items?.get(29)?.full_name ?: "")
        assertEquals(INDEX_29_IMAGE, data.items?.get(29)?.owner?.avatar_url ?: "")
        assertEquals(INDEX_29_DESCRIPTION, data.items?.get(29)?.description ?: "")
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .dispatcher(Dispatcher(me.pritom.dutta.domain.testutils.TestUtils.immediateExecutorService()))
            .retryOnConnectionFailure(true).build()
    }
}