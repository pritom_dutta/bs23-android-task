# BS23 Android Task



## Getting started

This is a simple app showing list and details page to click on the list item


## Tech Stack

* Kotlin based.
* MVVM Architecture
* Architecture component
* Repository Pattern
* ViewModel - UI related data holder, lifecycle aware.
* View Binding - bind UI components
* Navigation Component - navigate across, into, and back out from the different pieces of content within your app.
* Coroutines and Flow - for asynchronous.
* Retrofit2 - construct the REST APIs and paging network data.
* Moshi - A modern JSON library for Kotlin and Java.
* OkHttp3 - logging interceptor.
* Glide - loading images.
* Material-Components - Material design components.

<img src="../images/app_image.gif"/>