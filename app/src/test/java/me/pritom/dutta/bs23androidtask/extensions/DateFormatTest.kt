package me.pritom.dutta.bs23androidtask.extensions

import me.pritom.dutta.domain.utils.AppConstants
import org.junit.Assert.assertEquals
import org.junit.Test



/**
 * Created by Pritom Dutta on 23/3/24.
 */
class DateFormatTest {
    @Test
    fun valid_date_format_return() {
        val dateStr = "2024-03-21T14:40:31Z".getFromDateTime(
            AppConstants.yyyy_MM_dd_T_HH_mm_ss,
            AppConstants.MM_dd_yyyy_HH_ss
        )
        assertEquals("03-21-2024 14:31", dateStr)
    }

    @Test
    fun null_date_format_return() {
        val dateStr = "2024-03-21T14:40:31Z".getFromDateTime(
            AppConstants.MM_dd_yyyy_HH_ss,
            AppConstants.yyyy_MM_dd_T_HH_mm_ss
        )
        assertEquals(null, dateStr)
    }
}