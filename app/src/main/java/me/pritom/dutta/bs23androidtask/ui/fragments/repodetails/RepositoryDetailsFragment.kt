package me.pritom.dutta.bs23androidtask.ui.fragments.repodetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.chip.Chip
import me.pritom.dutta.bs23androidtask.R
import me.pritom.dutta.bs23androidtask.databinding.FragmentRepositoryDetailsBinding
import me.pritom.dutta.bs23androidtask.extensions.getFromDateTime
import me.pritom.dutta.bs23androidtask.extensions.loadImage
import me.pritom.dutta.bs23androidtask.extensions.viewBinding
import me.pritom.dutta.domain.model.RepoItem
import me.pritom.dutta.domain.utils.AppConstants


class RepositoryDetailsFragment : Fragment(R.layout.fragment_repository_details) {

    private val binding by viewBinding<FragmentRepositoryDetailsBinding>()
    private val args: RepositoryDetailsFragmentArgs by navArgs()
    private var details: RepoItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            details = args.repoDetails
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupData()
    }

    private fun setupData() {
        binding.apply {
            tvName.text = details?.full_name ?: ""
            tvDescription.text = details?.description ?: ""
            tvUpdateTime.text = (details?.updated_at ?: "")
                .getFromDateTime(
                    AppConstants.yyyy_MM_dd_T_HH_mm_ss,
                    AppConstants.MM_dd_yyyy_HH_ss
                )

            imgProfile.loadImage(details?.owner?.avatar_url ?: "")

            details?.topics?.forEach { name ->
                binding.topicsChip.addView(createTagChip(requireContext(), name))
            }
        }
    }

    private fun createTagChip(context: Context, chipName: String): Chip {
        return Chip(context).apply {
            text = chipName
            setChipBackgroundColorResource(R.color.black)
            isCloseIconVisible = false
            isClickable = false
            setTextColor(ContextCompat.getColor(context, R.color.white))
//            setTextAppearance(R.style.ChipTextAppearance)
        }

    }
}