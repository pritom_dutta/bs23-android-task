package me.pritom.dutta.bs23androidtask.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Pritom Dutta on 21/3/24.
 */
@HiltAndroidApp
class AndroidTaskApp: Application()