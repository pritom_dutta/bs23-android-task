package me.pritom.dutta.bs23androidtask.extensions

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat

/**
 * Created by Pritom Dutta on 22/3/24.
 */

@SuppressLint("SimpleDateFormat")
fun String.getFromDateTime(dateFormat: String, outFormat: String): String? {
    val input = SimpleDateFormat(dateFormat)
    val output = SimpleDateFormat(outFormat)
    try {
        val getAbbreviate = input.parse(this)    // parse input
        return output.format(getAbbreviate)    // format output
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null
}