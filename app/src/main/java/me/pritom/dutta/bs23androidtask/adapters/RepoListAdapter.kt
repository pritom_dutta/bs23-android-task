package me.pritom.dutta.bs23androidtask.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.pritom.dutta.bs23androidtask.adapters.base.BaseListAdapter
import me.pritom.dutta.domain.model.RepoItem

/**
 * Created by Pritom Dutta on 22/3/24.
 */
class RepoListAdapter(private val onClickItem: (RepoItem) -> Unit) : BaseListAdapter<RepoItem>(
    itemsSame = { old, new -> old.owner?.id == new.owner?.id },
    contentsSame = { old, new -> old == new }
) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        inflater: LayoutInflater,
        viewType: Int
    ) = RepoListViewHolder(inflater)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RepoListViewHolder -> {
                holder.bind(getItem(position), onClickItem)
            }
        }
    }
}