package me.pritom.dutta.bs23androidtask.extensions

import android.content.Context
import android.widget.Toast

/**
 * Created by Pritom Dutta on 22/3/24.
 */

fun Context.showToast(message: String, howLong: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, message, howLong).show()