package me.pritom.dutta.bs23androidtask.core.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Pritom Dutta on 21/3/24.
 */
@GlideModule
class AppGlideModule: AppGlideModule()