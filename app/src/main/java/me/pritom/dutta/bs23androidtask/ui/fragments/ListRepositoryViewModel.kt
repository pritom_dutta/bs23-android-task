package me.pritom.dutta.bs23androidtask.ui.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import me.pritom.dutta.bs23androidtask.BuildConfig
import me.pritom.dutta.domain.model.RepositoryResponse
import me.pritom.dutta.domain.repository.RepoListRepository
import me.pritom.dutta.domain.utils.NetworkResult
import javax.inject.Inject

/**
 * Created by Pritom Dutta on 22/3/24.
 */
@HiltViewModel
class ListRepositoryViewModel @Inject constructor(private val repository: RepoListRepository) :
    ViewModel() {

    private var _responseList =
        MutableStateFlow<NetworkResult<RepositoryResponse>>(NetworkResult.Loading())
    var responseList = _responseList.asStateFlow()

    init {
        fetchList()
    }

    private fun fetchList() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.fetchListRepository(BuildConfig.APPLICATION_ID)
                .collect {
                    _responseList.emit(it)
                }
        }
    }

}