package me.pritom.dutta.bs23androidtask.extensions

import android.widget.ImageView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import me.pritom.dutta.bs23androidtask.R

/**
 * Created by Pritom Dutta on 21/3/24.
 */

//
fun ImageView.loadImage(url: String?) {
    Glide.with(this).load(url)
        .placeholder(R.drawable.ic_image_searching_or_error)
        .error(R.drawable.ic_image_searching_or_error).into(this)
}
