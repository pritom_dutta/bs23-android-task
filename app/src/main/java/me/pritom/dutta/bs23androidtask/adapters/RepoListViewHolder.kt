package me.pritom.dutta.bs23androidtask.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import me.pritom.dutta.bs23androidtask.adapters.base.BaseViewHolder
import me.pritom.dutta.bs23androidtask.databinding.ItemRepoListBinding
import me.pritom.dutta.bs23androidtask.extensions.loadImage
import me.pritom.dutta.bs23androidtask.utils.setSafeOnClickListener
import me.pritom.dutta.domain.model.RepoItem

/**
 * Created by Pritom Dutta on 22/3/24.
 */
class RepoListViewHolder(inflater: LayoutInflater) : BaseViewHolder<ItemRepoListBinding>(
    binding = ItemRepoListBinding.inflate(inflater)
) {
    fun bind(data: RepoItem, onClickItem: (RepoItem) -> Unit) {
        itemView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        binding.apply {
            itemClick.setSafeOnClickListener {
                onClickItem.invoke(data)
            }
            tvName.text = data.full_name ?: ""
            tvDescription.text = data.description ?: ""
            imgProfile.loadImage(data.owner?.avatar_url ?: "")
        }
    }
}