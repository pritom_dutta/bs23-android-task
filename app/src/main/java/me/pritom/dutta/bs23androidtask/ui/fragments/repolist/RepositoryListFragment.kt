package me.pritom.dutta.bs23androidtask.ui.fragments.repolist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import me.pritom.dutta.bs23androidtask.R
import me.pritom.dutta.bs23androidtask.adapters.RepoListAdapter
import me.pritom.dutta.bs23androidtask.databinding.FragmentRepositoryListBinding
import me.pritom.dutta.bs23androidtask.extensions.safeCollectFlow
import me.pritom.dutta.bs23androidtask.extensions.showToast
import me.pritom.dutta.bs23androidtask.extensions.viewBinding
import me.pritom.dutta.bs23androidtask.ui.fragments.ListRepositoryViewModel
import me.pritom.dutta.bs23androidtask.utils.EqualSpacingItemDecoration
import me.pritom.dutta.domain.utils.NetworkResult


@AndroidEntryPoint
class RepositoryListFragment : Fragment(R.layout.fragment_repository_list) {

    private val binding by viewBinding<FragmentRepositoryListBinding>()
    private val viewModel: ListRepositoryViewModel by viewModels()
    private lateinit var mRepoListAdapter: RepoListAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        //api call & response
        responseFromApiForList()
    }

    private fun setupUI() {
        mRepoListAdapter = RepoListAdapter { repoDetails ->
            //Going to Details Page
            val action =
                RepositoryListFragmentDirections.actionRepositoryListFragmentToRepositoryDetailsFragment()
            action.repoDetails = repoDetails
            findNavController().navigate(action)
        }
        //setup Repo List RecyclerView
        binding.rvRepoList.apply {
            addItemDecoration(EqualSpacingItemDecoration(40))
            adapter = mRepoListAdapter
        }
    }

    private fun loadingHandle(isLoading: Boolean) {
        binding.remoteViewLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    //Api Response
    private fun responseFromApiForList() {
        lifecycleScope.launch {
            safeCollectFlow(flow = viewModel.responseList) { result ->
                when (result) {
                    is NetworkResult.Success -> {
                        loadingHandle(false)
                        result.data?.let { response ->
                            mRepoListAdapter.submitList(response.items)
                        }
                    }

                    is NetworkResult.Error -> {
                        loadingHandle(false)
                         requireContext().showToast(message = result.message.toString())
                    }

                    is NetworkResult.Loading -> {
                        loadingHandle(true)
                    }
                }
            }
        }
    }

}